package com.guijapa.aulaoandroid.Modelos;

public class User {

    // Atributos do usuário acessíveis atraves do "." por exemplo user.name;
    public String name;
    public Integer age;

    // Método do usuário que também é acessível com ".";
    public String print() {
        return "Meu nome é " + name + " e tenho " + age + " anos.";
    }

}
