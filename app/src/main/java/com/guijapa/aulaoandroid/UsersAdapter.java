package com.guijapa.aulaoandroid;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.guijapa.aulaoandroid.Modelos.User;

import java.util.ArrayList;

public class UsersAdapter extends RecyclerView.Adapter<UserViewHolder> {

    public ArrayList<User> users = new ArrayList<User>();

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_user, parent, false);
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        User user = users.get(position);
        holder.setUser(user);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }


}

class UserViewHolder extends RecyclerView.ViewHolder {

    TextView nameTextView;
    TextView ageTextView;

    public UserViewHolder(View itemView) {
        super(itemView);
        nameTextView = (TextView) itemView.findViewById(R.id.nameTextView);
        ageTextView = (TextView) itemView.findViewById(R.id.ageTextView);
    }

    public void setUser(User user) {
        nameTextView.setText(user.name);
        ageTextView.setText(user.age.toString());
    }


}
