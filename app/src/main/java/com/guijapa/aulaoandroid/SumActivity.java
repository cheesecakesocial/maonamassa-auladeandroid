package com.guijapa.aulaoandroid;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SumActivity extends AppCompatActivity {

    Integer sum = 0;
    TextView sumText;
    EditText firstNumberEdit;
    EditText secondNumberEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sum_numbers);
        sumText = (TextView) findViewById(R.id.sumText);
        firstNumberEdit = (EditText) findViewById(R.id.firstNumberEdit);
        secondNumberEdit = (EditText) findViewById(R.id.secondNumberEdit);
    }

    public void sumNumbers(View view) {
        Integer firstNum = Integer.parseInt(firstNumberEdit.getText().toString());
        Integer secondNum = Integer.parseInt(secondNumberEdit.getText().toString());
        sum = firstNum % secondNum;
        sumText.setText(sum.toString());
    }

}
