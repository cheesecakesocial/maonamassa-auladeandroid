package com.guijapa.aulaoandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    // Variáveis e atributos de MainActivity
    Integer count = 0;
    TextView countText;
    Button countButton;
    TextView titleText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView diz para a Activity qual arquivo XML (layout) vai ser usado
        setContentView(R.layout.activity_main);
        // findViewById retorna um elemento específico do XML para a Activity
        countText = (TextView) findViewById(R.id.countText);
        titleText = (TextView) findViewById(R.id.title);
        countButton = (Button) findViewById(R.id.button);
    }

    public void buttonClicked(View sender) {
        // Condicional if/else
        if (count >= 10) {
            count = 0;
        } else {
            count = count + 1;
        }
    }

    public void loopClicked(View view) {
        // Como iniciar uma nova Activity ou "ir para outra tela".
        Intent intent = new Intent(this, UsersActivity.class);
        startActivity(intent);
    }

}
