package com.guijapa.aulaoandroid;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.guijapa.aulaoandroid.Modelos.User;

import java.util.ArrayList;

public class UsersActivity extends AppCompatActivity {

    EditText nameEditText;
    EditText ageEditText;
    Button createUserButton;
    TextView userTextView;
    RecyclerView usersListView;
    UsersAdapter usersAdapter = new UsersAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);

        nameEditText = (EditText) findViewById(R.id.nameEditText);
        ageEditText = (EditText) findViewById(R.id.ageEditText);
        createUserButton = (Button) findViewById(R.id.createUserButton);
        usersListView = (RecyclerView) findViewById(R.id.usersListView);
        usersListView.setAdapter(usersAdapter);

        // Listeners
        createUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = new User();
                user.name = nameEditText.getText().toString();
                user.age = Integer.parseInt(ageEditText.getText().toString());
                usersAdapter.users.add(0, user);
                usersAdapter.notifyDataSetChanged();
            }
        });


    }

    public void listUsers() {
        String usersString = "";
        // For é uma expressão para percorrer todos os elementos da lista
//        for (User user: users) {
//            usersString += user.print() + "\n";
//        }
//        userTextView.setText(usersString);
    }

}
